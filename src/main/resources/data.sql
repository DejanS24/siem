insert into siem.user (id, username, password) values (250, 'user', '$2a$04$Amda.Gm4Q.ZbXz9wcohDHOhOBaNQAkSS1QO26Eh8Hovu3uzEpQvcq');
insert into siem.user (id, username, password) values (251, 'user1', '$2a$04$Amda.Gm4Q.ZbXz9wcohDHOhOBaNQAkSS1QO26Eh8Hovu3uzEpQvcq');

insert into siem.log_user (id, username, password,failed_attempts,inactive,risk) values (250, 'user', '$2a$04$Amda.Gm4Q.ZbXz9wcohDHOhOBaNQAkSS1QO26Eh8Hovu3uzEpQvcq',0,15,0);
insert into siem.log_user (id, username, password,failed_attempts,inactive,risk) values (251, 'user1', '$2a$04$Amda.Gm4Q.ZbXz9wcohDHOhOBaNQAkSS1QO26Eh8Hovu3uzEpQvcq',3,95,0);


insert into siem.role (id, name) values (250, 'Admin');
insert into siem.role (id, name) values (251, 'Operator');

insert into siem.permission (id, name) values (250, 'logView');

insert into siem.user_role (role_id, user_id) values (250, 250);
insert into siem.user_role (role_id, user_id) values (251, 251);

insert into siem.role_permission (role_id, permission_id) values (250, 250);

insert into siem.ip_address(id,ip,is_malicious) values(150, '192.168.1.1', false);
insert into siem.log (id, text, status, ip_id, timestamp, platform,type,user_id) values (150,'message', 1,150,'2008-11-11', 'windows', 2,250);
insert into siem.log (id, text, status, ip_id, timestamp, platform,type,user_id) values (151,'MESSAGE', 1,150,'2008-11-11', 'windows', 2,250);

insert into siem.alarm(id, from_system, message, priority, timestamp) values (150,false,"Alarm log",2, '2019-10-10')
