package com.project.siem.domain;

public enum UserType {
	ADMIN, OPERATOR
}
