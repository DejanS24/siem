package com.project.siem.domain.dynamic_rule;

import com.project.siem.DTO.DynamicRuleDTO;

public class Rule {

	private String name;
	private Condition condition;
	private Integer occurrences;
	private Interval interval;

	public Rule(String name, Condition condition, Integer occurrences, Interval interval) {
		super();
		this.name = name;
		this.condition = condition;
		this.occurrences = occurrences;
		this.interval = interval;
	}

	public Rule() {
		super();
	}
	
	public Rule(DynamicRuleDTO dynamicRuleDTO) {
		this.name = dynamicRuleDTO.getName();
		this.occurrences = dynamicRuleDTO.getOccurrences();
		this.interval = new Interval(dynamicRuleDTO.getInterval(), dynamicRuleDTO.getUnit());
		this.condition = new Condition(dynamicRuleDTO.getConditions(), dynamicRuleDTO.getNot());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Condition getCondition() {
		return condition;
	}

	public void setCondition(Condition condition) {
		this.condition = condition;
	}

	public Integer getOccurrences() {
		return occurrences;
	}

	public void setOccurrences(Integer occurrences) {
		this.occurrences = occurrences;
	}

	public Interval getInterval() {
		return interval;
	}

	public void setInterval(Interval interval) {
		this.interval = interval;
	}
	
	public String getOccurrencesAsString() {
		return String.valueOf(this.occurrences);
	}
	
	public String getIntervalAsString() {
		return this.interval.toString();
	}
	
	public String getConditionsAsString() {
		return this.condition.toString();
	}

}
