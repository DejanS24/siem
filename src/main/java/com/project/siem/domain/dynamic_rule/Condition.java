package com.project.siem.domain.dynamic_rule;

import java.util.ArrayList;

public class Condition {

	private ArrayList<String> conditionsAndOperators;
	private Boolean not;

	public ArrayList<String> getConditionsAndOperators() {
		return conditionsAndOperators;
	}

	public void setConditionsAndOperators(ArrayList<String> conditionsAndOperators) {
		this.conditionsAndOperators = conditionsAndOperators;
	}

	public Boolean getNot() {
		return not;
	}

	public void setNot(Boolean not) {
		this.not = not;
	}

	public Condition() {
		super();
	}

	public Condition(ArrayList<String> conditionsAndOperators, Boolean not) {
		super();
		this.conditionsAndOperators = conditionsAndOperators;
		this.not = not;
	}

	@Override
	public String toString() {
		String conditionStr = "";
		if (this.not) {
			conditionStr += "not (";
		}
		for (String str : this.conditionsAndOperators) {
			conditionStr += str + " ";
		}
		if (this.not) {
			conditionStr += ")";
		}
		return conditionStr;
	}

}
