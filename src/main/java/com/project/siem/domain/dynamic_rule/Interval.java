package com.project.siem.domain.dynamic_rule;

public class Interval {

	private Integer value;
	private String unit;

	public Interval(Integer value, String unit) {
		super();
		this.value = value;
		this.unit = unit;
	}

	public Interval() {
		super();
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Override
	public String toString() {
		if (this.unit.equals("days")) {
			return "minusDays("+this.value+")";
		}else if (this.unit.equals("hours")) {
			return "minusHours("+this.value+")";
		}else if (this.unit.equals("minutes")) {
			return "minusMinutes("+this.value+")";
		}else {
			return "minusSeconds("+this.value+")";
		}
	}
	
	

}
