package com.project.siem.domain.facts;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class LogUser {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	public enum UserRisk{
		LOW, MODERATE, HIGH, EXTREME
	};
	
	@Column
	private String username;
	
	@Column
	private String password;

	@Column
	private int failedAttempts;
	
	@Column
	private UserRisk risk;
	
	@Column
	private int inactive;
	
	public LogUser() {
	}
	
	
	public LogUser(String username, String password,  int failedAttempts, UserRisk risk, int inactive) {
		super();
		this.username = username;
		this.password = password;
		this.failedAttempts = failedAttempts;
		this.risk = risk;
		this.inactive = inactive;
	}
	
	public LogUser(String username, String risk, int inactive) {
		super();
		this.username = username;
		System.out.println(risk);
		this.risk = UserRisk.valueOf(risk);
		this.inactive = inactive;
	}
	
	

	public int getInactive() {
		return inactive;
	}
	public void setInactive(int inactive) {
		this.inactive = inactive;
	}
	public UserRisk getRisk() {
		return risk;
	}
	public void setRisk(UserRisk risk) {
		this.risk = risk;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getFailedAttempts() {
		return failedAttempts;
	}
	public void setFailedAttempts(int failedAttempts) {
		this.failedAttempts = failedAttempts;
	}
	
	
	
}
