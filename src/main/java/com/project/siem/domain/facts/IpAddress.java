package com.project.siem.domain.facts;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class IpAddress {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column
	private Boolean isMalicious;

	@Column
	private String ip;
	
	
	

	public IpAddress() {
		super();
	}

	public IpAddress(String ip, Boolean isM) {
		super();
		this.ip = ip;
		this.isMalicious = isM;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public Boolean getIsMalicious() {
		return isMalicious;
	}

	public void setIsMalicious(Boolean isMalicious) {
		this.isMalicious = isMalicious;
	}

}
