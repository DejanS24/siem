package com.project.siem.domain.facts;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Log {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column
	private String text;
	@Column
	private Status status;
	


	@ManyToOne
	private IpAddress ip;

	@Column
	private LocalDateTime timestamp;

	@Column
	private String platform;
	
	
	@Column
	private LogType type;
	@ManyToOne
	private LogUser user;

	public enum LogType {
		ERROR, LOGIN, ANTIVIRUS, PAYMENT
	};

	public enum Status {
		SUCCESS, ERROR, WARNING
	};

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public Log(String text, Status status, LogType type, LogUser user, IpAddress ip, LocalDateTime timestamp) {
		super();
		this.text = text;
		this.status = status;
		this.type = type;
		this.user = user;
		this.ip = ip;
		this.timestamp = timestamp;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Log(String text, LogType type, LogUser user, IpAddress ip, LocalDateTime timestamp) {
		super();
		this.text = text;
		this.type = type;
		this.user = user;
		this.ip = ip;
		this.timestamp = timestamp;
	}

	public IpAddress getIp() {
		return ip;
	}

	public void setIp(IpAddress ip) {
		this.ip = ip;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public Log() {
	}

	public Log(IpAddress ip, String platform, LocalDateTime parse, String type, String status, String text, LogUser user) {
		this.ip = ip;
		this.platform = platform;
		this.timestamp = parse;
		this.type = LogType.valueOf(type);
		this.status = Status.valueOf(status);
		this.text = text;
		this.user = user;
	}

	public LogUser getUser() {
		return user;
	}

	public void setUser(LogUser user) {
		this.user = user;
	}

	public String getText() {
		return text;
	}

	public LogType getType() {
		return type;
	}

	public void setType(LogType type) {
		this.type = type;
	}

	public void setText(String text) {
		this.text = text;
	}

}