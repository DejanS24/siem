package com.project.siem.domain.facts;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Alarm {
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	public enum TypeOfAlarm{
		LOGIN, ANTIVIRUS, DOS, MALICIOUSIP, ERROR, PAYMENT,  BT
	}
	
	@Column
	private String message;

	@Column
	private String priority;

	@Column
	private LocalDateTime timestamp;

	@Column
	private Boolean fromSystem;
	
	@Column
	private TypeOfAlarm type;



	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Log> logs;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private IpAddress ip;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private LogUser user;

	public Alarm() {
		
	}
	
	public Alarm(String message, LocalDateTime timestamp, List<Log> logs) {
		this.message = message;
		this.timestamp = timestamp;
		this.logs = logs;
		this.fromSystem = true;
	}
	public Alarm(String message, LocalDateTime timestamp, List<Log> logs, LogUser user) {
		this.message = message;
		this.timestamp = timestamp;
		this.logs = logs;
		this.fromSystem = true;
		this.user = user;
	}
	
	public Alarm(String message, LocalDateTime timestamp, List<Log> logs, IpAddress ipAddress) {
		this.message = message;
		this.timestamp = timestamp;
		this.logs = logs;
		this.fromSystem = true;
		this.ip = ipAddress;
	}

	public Alarm(Long id, String message, LocalDateTime timestamp, List<Log> logs, IpAddress ipAddress, LogUser user, TypeOfAlarm ta) {
		this.id = id;
		this.message = message;
		this.timestamp = timestamp;
		this.logs = logs;
		this.fromSystem = true;
		this.ip = ipAddress;
		this.user = user;
		this.type = ta;
	}
	
	public Alarm(String message, LocalDateTime timestamp, List<Log> logs, IpAddress ipAddress, LogUser user) {
		this.message = message;
		this.timestamp = timestamp;
		this.logs = logs;
		this.fromSystem = true;
		this.user = user;
		this.ip = ipAddress;

	}
	
	public IpAddress getIp() {
		return ip;
	}

	public void setIp(IpAddress ipAddress) {
		this.ip = ipAddress;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public Boolean getFromSystem() {
		return fromSystem;
	}

	public void setFromSystem(Boolean fromSystem) {
		this.fromSystem = fromSystem;
	}

	public List<Log> getLogs() {
		return logs;
	}

	public void setLogs(List<Log> logs) {
		this.logs = logs;
	}

	public LogUser getUser() {
		return user;
	}

	public void setUser(LogUser user) {
		this.user = user;
	}
	public TypeOfAlarm getType() {
		return type;
	}

	public void setType(TypeOfAlarm type) {
		this.type = type;
	}

}
