package com.project.siem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.siem.domain.facts.Alarm;

public interface AlarmRepository extends JpaRepository<Alarm, Long>{
	
}
