package com.project.siem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.siem.domain.facts.Log;
import com.project.siem.domain.facts.Log.LogType;



public interface LogRepository extends JpaRepository<Log, Long>{
	List<Log> findByType(LogType lg);
	List<Log> findByStatus(String status);
	List<Log> findByPlatform(String platform);
	
}
