package com.project.siem.controller;

import com.project.siem.DTO.DynamicRuleDTO;
import com.project.siem.DTO.LoginRuleDTO;
import com.project.siem.service.RulesService;

import org.apache.maven.shared.invoker.MavenInvocationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@Controller
@RequestMapping("/rules")
public class RulesController {


	private final RulesService rulesService;

	@Autowired
	public RulesController(RulesService rulesService){
		this.rulesService = rulesService;
	}
	
	@RequestMapping(value = "/dynamic_rule", method = RequestMethod.POST)
	public ResponseEntity<String> createDynamicRule(@RequestBody DynamicRuleDTO dynamicRuleDTO) {
		String response;
		try {
			response = rulesService.createDynamicRule(dynamicRuleDTO);
		} catch (MavenInvocationException e) {
			response = "Failed to update rule system";
			e.printStackTrace();
		}
        return new ResponseEntity<String>(response, HttpStatus.OK);
        
	}
	
	@RequestMapping(value = "/login_rule", method = RequestMethod.POST)
	public ResponseEntity<String> login(@RequestBody LoginRuleDTO loginRuleDTO) {
		String response = rulesService.loginRule(loginRuleDTO);
        return new ResponseEntity<String>(response, HttpStatus.OK);
        
	}
}
