package com.project.siem.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.project.siem.DTO.LogDTO;
import com.project.siem.domain.facts.Alarm;
import com.project.siem.domain.facts.Log;
import com.project.siem.service.LogService;




@RestController
@RequestMapping(value = "/logView")
public class LogController {
	
	private final LogService logService;
	
	@Autowired
	public LogController(LogService logService) {
		this.logService = logService;
	}
	
	@RequestMapping(value = "/getAlarms", method = RequestMethod.GET)
	public ResponseEntity<List<Alarm>> getAlarms(){
		List<Alarm> alarms = logService.getAlarms();
		if(alarms == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);     
		}
		return new ResponseEntity<List<Alarm>>(alarms, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/acceptLog", method = RequestMethod.POST)
	public ResponseEntity<String> acceptLogs(@RequestBody LogDTO logDTO) {
		Log log = logService.acceptLog(logDTO);
		System.out.println(log);
        return new ResponseEntity<String>("List of logs", HttpStatus.OK);     
	}
	

	@RequestMapping(value = "/getLogs", method = RequestMethod.GET)
	public ResponseEntity<List<Log>> getLogs(){
		System.out.println("here");
		List<Log> logs = logService.getAllLogs();
		
		return new ResponseEntity<List<Log>>(logs, HttpStatus.OK);
	}
	
	
	
	
		
	
	
	
	
	
}
