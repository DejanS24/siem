package com.project.siem.controller;

import javax.validation.Valid;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.project.siem.DTO.LoginDTO;
import com.project.siem.security.TokenUtils;
import com.project.siem.service.UserService;
import com.project.siem.domain.User;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	private UserService userService;
		
	@Autowired
	TokenUtils tokenUtils;
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<String> login(@RequestBody @Valid LoginDTO loginDTO) {
        try {
        	// Za parametre funkcije treba napraviti wrapper funkcije
        	// i njihova validnost se proverava sa @valid
        	UsernamePasswordAuthenticationToken token = 
        			new UsernamePasswordAuthenticationToken(
					loginDTO.getUsername(), loginDTO.getPassword());
            Authentication authentication = authenticationManager.authenticate(token);            
            SecurityContextHolder.getContext().setAuthentication(authentication);
            // Reload user details so we can generate token
            UserDetails details = userDetailsService.
            		loadUserByUsername(loginDTO.getUsername());
            return new ResponseEntity<String>(
            		tokenUtils.generateToken(details), HttpStatus.OK);
        } catch (Exception ex) {
        	System.out.println(ex.getMessage());
            return new ResponseEntity<String>("Invalid login", HttpStatus.BAD_REQUEST);
        }
	}
	
	@RequestMapping(value = "/getUsers", method = RequestMethod.GET)
	@PreAuthorize("hasAuthority('getUsers')")
	public ResponseEntity<List<String>> getUsers(){
		List<String> users =  userService.getUsers();
		return new ResponseEntity<List<String>>(users, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public ResponseEntity<String> testAgent(){
		return new ResponseEntity<String>("TEST USPESAN!!", HttpStatus.OK);
		
	}
	
	public void preventXMLAttack() {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		String FEATURE = null;
		try {
			FEATURE = "http://apache.org/xml/features/disallow-doctype-decl";
		    dbf.setFeature(FEATURE, true);
		    FEATURE = "http://apache.org/xml/features/nonvalidating/load-external-dtd";
		    dbf.setFeature(FEATURE, false);
		    dbf.setXIncludeAware(false);
		    dbf.setExpandEntityReferences(false);

		}catch(ParserConfigurationException e) {
			 System.out.println("ParserConfigurationException was thrown. The feature '" + FEATURE 
					    + "' is probably not supported by your XML processor.");
			
		}
	}
	
}
