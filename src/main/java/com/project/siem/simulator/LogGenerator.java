//package com.project.siem.simulator;
//
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import java.util.Random;
//import com.google.common.net.InetAddresses;
//
//
//public class LogGenerator {
//	private static Logger logger = LogManager.getLogger(LogGenerator.class);
//	
//	public static String generateRandomId() {
//		Random rand = new Random();
//		String id = String.valueOf(rand.nextInt(100) + 1);
//		return id;
//	}
//	
//	public static String generateRandomIp() {
//		Random rand = new Random();
//		String ipString = InetAddresses.fromInteger(rand.nextInt()).getHostAddress();
//		return ipString;
//	}
//	
//	public static void createLog() {
//		String userId = "";
//		String ip = "";
//		for(int i = 0; i < 10;i++) {
//			Random rand = new Random();
//			userId = generateRandomId();
//			ip = generateRandomIp();
//			int j = rand.nextInt(7);
//			generateMessage(j,userId,ip);
//			
//		}
//	}
//
//	private static void generateMessage(int j,String id,String ip) {
//		String message = "";
//		if(j==0) {
//			message = "User id: " + id + " User ip: " + ip + " status = success action=LOGIN";
//			logger.info(message);
//		}else if(j == 1) {
//			message = "User id: " + id + " User ip: " + ip + " status = success action=LOGOUT";
//			logger.info(message);
//		}else if(j==2) {
//			message = "Invalid data" + " status = failure action=FAILED_LOGIN";
//			logger.error(message);
//		}else if(j==3) {
//			message = "Antivirus detected some malicious files" + " status = warning action=ANTIVIRUS";
//			logger.warn(message);
//			message = "Dangerous files have been removed" + " status = success action=ANTIVIRUS";
//			logger.info(message);
//		}else if(j==4) {
//			message = "User id: " + id + " User ip: " + ip + " status = success action=REGISTER";
//			logger.info(message);
//		}else if(j==5) {
//			message = "SQL MESSAGE";
//			logger.info(message);
//		}else {
//			
//		}
//	}
//	
//}
