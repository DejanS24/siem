package com.project.siem.DTO;

import java.time.LocalDateTime;



public class LogDTO {
	private int id;
	private LocalDateTime timestamp;
	private String ip;
	private String type;
	private String status;
	private String text;
	
	
	
	public LogDTO() {
		super();
	}
	public LogDTO(LocalDateTime timestamp, String ip, String type, String status, String text) {
		super();
		this.timestamp = timestamp;
		this.ip = ip;
		this.type = type;
		this.status = status;
		this.text = text;
	}
	public LocalDateTime getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	

}
