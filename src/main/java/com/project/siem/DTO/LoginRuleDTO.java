package com.project.siem.DTO;

public class LoginRuleDTO {

    private String inactiveDays;
    private String numberOfTries;
    private String numberOfDays;
    private String timePeriod;

    public LoginRuleDTO(String inactiveDays, String numberOfTries, String numberOfDays, String timePeriod) {
        super();
        this.inactiveDays = inactiveDays;
        this.numberOfTries = numberOfTries;
        this.numberOfDays = numberOfDays;
        this.timePeriod = timePeriod;
    }

    public LoginRuleDTO() {
        super();
    }

    public String getInactiveDays() {
        return inactiveDays;
    }

    public void setInactiveDays(String inactiveDays) {
        this.inactiveDays = inactiveDays;
    }

    public String getNumberOfTries() {
        return numberOfTries;
    }

    public void setNumberOfTries(String numberOfTries) {
        this.numberOfTries = numberOfTries;
    }

    public String getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(String numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public String getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(String timePeriod) {
        this.timePeriod = timePeriod;
    }

}
