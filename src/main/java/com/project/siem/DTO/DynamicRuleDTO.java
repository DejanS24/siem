package com.project.siem.DTO;

import java.util.ArrayList;
import java.util.List;

import com.github.rkpunjal.sqlsafe.SQLInjectionSafe;

public class DynamicRuleDTO {

	private @SQLInjectionSafe String name;
	private Integer occurrences;
	private Integer interval;
	private String unit;
	private ArrayList<String> conditions;
	private Boolean not;

	public DynamicRuleDTO(String name, Integer occurrences, Integer interval, String unit, ArrayList<String> conditions,
			Boolean not) {
		super();
		this.name = name;
		this.occurrences = occurrences;
		this.interval = interval;
		this.unit = unit;
		this.conditions = conditions;
		this.not = not;
	}

	public DynamicRuleDTO() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOccurrences() {
		return occurrences;
	}

	public void setOccurrences(Integer occurrences) {
		this.occurrences = occurrences;
	}

	public Integer getInterval() {
		return interval;
	}

	public void setInterval(Integer interval) {
		this.interval = interval;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public ArrayList<String> getConditions() {
		return conditions;
	}

	public void setConditions(ArrayList<String> conditions) {
		this.conditions = conditions;
	}

	public Boolean getNot() {
		return not;
	}

	public void setNot(Boolean not) {
		this.not = not;
	}

}
