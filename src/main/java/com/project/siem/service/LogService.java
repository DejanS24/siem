package com.project.siem.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.kie.api.event.rule.DebugAgendaEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.project.siem.DTO.LogDTO;
import com.project.siem.domain.UserType;
import com.project.siem.domain.facts.Alarm;
import com.project.siem.domain.facts.IpAddress;
import com.project.siem.domain.facts.Log;
import com.project.siem.domain.facts.Log.LogType;
import com.project.siem.domain.facts.Log.Status;
import com.project.siem.domain.facts.LogUser;
import com.project.siem.domain.facts.LogUser.UserRisk;
import com.project.siem.repository.AlarmRepository;
import com.project.siem.repository.LogRepository;
import com.project.siem.repository.PermissionRepository;


@Service
public class LogService {

    @Autowired
    private KieContainer kieContainer;
	
	@Autowired
	private LogRepository logRepository;
	
	@Autowired
	private  AlarmRepository alarmRepository; 
	
	private KieSession kieSession;
	
	public List<Log> getAllLogs() {
		List<Log> allLogs = logRepository.findAll();
		
		return allLogs;
	}
	
	
	 @EventListener(ApplicationReadyEvent.class)
	 public void initializeSessions() {
	        kieSession = kieContainer.newKieSession();
            kieSession.setGlobal("logService", this);
            kieSession.addEventListener(new AlarmListener(kieSession));
            List<Log> logs = new ArrayList<Log>();
            LogUser user  = new LogUser("misk", "123", 3, UserRisk.LOW, 95);
            Log l = new Log("TEXT", Status.WARNING, LogType.LOGIN, user, new IpAddress("192.168.1.1", false), LocalDateTime.now());
    //	        Log l2 = new Log(ip, "windows", LocalDateTime.now().minusSeconds(5), "LOGIN", "WARNING", "check",
    //					new User("misk1", "LOW", 95));
    //	        Log l3 = new Log("TEXT3", Status.WARNING, LogType.LOGIN, user, new IpAddress("192.168.1.3"), LocalDateTime.now());
    //	        Log l4 = new Log("TEXT3", Status.WARNING, LogType.LOGIN, user, new IpAddress("192.168.1.4"), LocalDateTime.now());
    //	        Log l5 = new Log("TEXT3", Status.WARNING, LogType.LOGIN, user, new IpAddress("192.168.1.5"), LocalDateTime.now());
    //	        logs.add(l);
    //	        logs.add(l2);
    //	        logs.add(l3);
    //	        logs.add(l4);
    //	        logs.add(l);
            System.out.println("Pokrenuto");
            kieSession.insert(l);
            kieSession.fireAllRules();
            kieSession.dispose();
	 }

	public Log acceptLog(LogDTO logDTO) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Alarm> getAlarms() {
		List<Alarm> alarms = alarmRepository.findAll();
		return alarms;
	}
	
	public void saveAlarm(Alarm a) {
		alarmRepository.save(a);
	}
	
	

}
