package com.project.siem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.siem.domain.facts.Alarm;
import com.project.siem.repository.AlarmRepository;

@Service
public class AlarmService {
	
	@Autowired
	private AlarmRepository alarmRepository;
	public List<Alarm> getAllAlarms() {
		List<Alarm> alarms = alarmRepository.findAll();
		return alarms;
	}

}
