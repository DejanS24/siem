package com.project.siem.service;

import org.kie.api.event.rule.AfterMatchFiredEvent;
import org.kie.api.event.rule.DebugAgendaEventListener;
import org.kie.api.event.rule.DefaultAgendaEventListener;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.api.runtime.rule.QueryResultsRow;

import com.project.siem.domain.facts.Alarm;
import com.project.siem.repository.AlarmRepository;


public class AlarmListener extends DefaultAgendaEventListener {
	
	private KieSession kieSession;
	private AlarmRepository alarmRepository;
	

	public AlarmListener(KieSession kieSession) {
		  this.kieSession = kieSession;
	}
	
	@Override
	   public void afterMatchFired(AfterMatchFiredEvent event) {
		QueryResults results = kieSession.getQueryResults("Alarms");
        // save new alarms
        for(QueryResultsRow queryResult : results){
           Alarm a = (Alarm) queryResult.get("$a");
           System.out.println("IM HERE");
           alarmRepository.save(a);
        }
	}
}
