package com.project.siem.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.project.siem.domain.Permission;
import com.project.siem.domain.Role;
import com.project.siem.domain.User;
import com.project.siem.repository.UserRepository;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

  @Autowired
  private UserRepository userRepository;

  @Override
  @Transactional
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = userRepository.findByUsername(username);
    
    try {
    System.out.println(user.getUsername() + "<- Nadjeni ->" + user.getPassword());	
    }catch(Exception e) {
    	
    }

    if (user == null) {
      throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
    } else {
//    	List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
//    	for (UserAuthority ua: user.getUserAuthorities()) {
//    		grantedAuthorities.add(
//    			new SimpleGrantedAuthority(ua.getAuthority().getName()));
//    	}
    	
    	//Java 1.8 way   	
//    	List<GrantedAuthority> grantedAuthorities = user.getRoles().
//    			stream()
//                .map(authority -> new SimpleGrantedAuthority(authority.getName()))
//                .collect(Collectors.toList());
    	
    	List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        for (Role role : user.getRoles()) {
            for (Permission permission : role.getPermissions()) {
                grantedAuthorities.add(new SimpleGrantedAuthority(permission.getName()));
            }
        }
    	return new org.springframework.security.core.userdetails.User(
    		  user.getUsername(),
    		  user.getPassword(),
    		  grantedAuthorities);
    }
  }

}
