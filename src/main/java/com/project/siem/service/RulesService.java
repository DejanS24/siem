package com.project.siem.service;

import com.project.siem.DTO.DynamicRuleDTO;
import com.project.siem.DTO.LoginRuleDTO;
import com.project.siem.domain.dynamic_rule.Rule;
import com.project.siem.util.RuleSystemUpdater;

import org.apache.maven.shared.invoker.MavenInvocationException;
import org.drools.template.DataProvider;
import org.drools.template.DataProviderCompiler;
import org.drools.template.objects.ArrayDataProvider;
import org.kie.api.runtime.KieContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class RulesService {

    private final KieContainer kieContainer;

    @Autowired
    public RulesService(KieContainer kieContainer) {
        this.kieContainer = kieContainer;
    }
    
    public String createDynamicRule(DynamicRuleDTO dynamicRuleDTO) throws MavenInvocationException {
    	InputStream template = RulesService.class.getResourceAsStream("/sbnz/template/log.drt");
    	
    	Rule dynamicRule = new Rule(dynamicRuleDTO);

        DataProvider dataProvider = new ArrayDataProvider(new String[][] {
                new String[] {dynamicRule.getName(),
                        dynamicRule.getConditionsAsString(),
                        dynamicRule.getOccurrencesAsString(),
                        dynamicRule.getIntervalAsString()}
        });

        DataProviderCompiler converter = new DataProviderCompiler();
        String drl = converter.compile(dataProvider, template);
        
        int filesNum = checkRuleFiles();
        System.out.println(filesNum);
        
        writeRules(drl, "C:/Users/Dejan/Documents/drools-spring-kjar/src/main/resources/sbnz/integracija/dynamicRule"+filesNum+".drl");
        System.out.println(drl);
        RuleSystemUpdater.runInstall();
        return "success";
    }


    public String loginRule(LoginRuleDTO loginRuleDTO) {
        InputStream template = RulesService.class.getResourceAsStream("/sbnz/template/login-rules.drt");

        DataProvider dataProvider = new ArrayDataProvider(new String[][] {
                new String[] {loginRuleDTO.getInactiveDays(),
                        loginRuleDTO.getNumberOfTries(),
                        loginRuleDTO.getNumberOfDays(),
                        loginRuleDTO.getTimePeriod()}
        });

        DataProviderCompiler converter = new DataProviderCompiler();
        String drl = converter.compile(dataProvider, template);
        writeRules(drl, "C:/Users/Dejan/Documents/drools-spring-kjar/src/main/resources/sbnz/integracija/rules2.drl");
        System.out.println(drl);
        return "success";
    }

    private void writeRules(String s,String path) {
        File file = new File(path);
        FileWriter fr = null;
        BufferedWriter br = null;
        try {
            fr = new FileWriter(file);
            br = new BufferedWriter(fr);
            br.write(s);
        }catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                br.close();
                fr.close();
            }catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    private int checkRuleFiles() {
    	try (Stream<Path> walk = Files.walk(Paths.get("C:\\Users\\Dejan\\Documents\\drools-spring-kjar\\src\\main\\resources\\sbnz\\integracija"))) {

    		List<String> result = walk.filter(Files::isRegularFile)
    				.map(x -> x.toString()).collect(Collectors.toList());

    		result.forEach(System.out::println);
    		return result.size();

    	} catch (IOException e) {
    		e.printStackTrace();
    		return -1;
    	}
    }
}
