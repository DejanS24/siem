package com.project.siem.service;

import com.project.siem.domain.User;
import com.project.siem.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    
    public List<String> getUsers(){
		List<User> users = userRepository.findAll();
		System.out.println(users);
		List<String> usernames = new ArrayList<String>();
		for (User u : users) {
			usernames.add(u.getUsername());
		}
		return usernames;
	}
    
    
	public void setUserRisk(User user) {
		userRepository.save(user);
	}
}
