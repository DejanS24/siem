package com.project.siem.util;

import java.io.File;
import java.util.Collections;

import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.DefaultInvoker;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.Invoker;
import org.apache.maven.shared.invoker.MavenInvocationException;


public class RuleSystemUpdater {
	
	public static void runInstall() throws MavenInvocationException {
	
		InvocationRequest request = new DefaultInvocationRequest();
		request.setPomFile( new File ("C:\\Users\\Dejan\\Documents\\drools-spring-kjar\\pom.xml"));
		request.setGoals(Collections.singletonList("install"));
		
		Invoker invoker = new DefaultInvoker();
		invoker.execute(request);
	}
}
