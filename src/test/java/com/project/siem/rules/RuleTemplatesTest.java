package com.project.siem.rules;

import org.junit.Test;
import java.io.InputStream;
import org.drools.template.DataProvider;
import org.drools.template.objects.ArrayDataProvider;
import org.drools.template.DataProviderCompiler;
import org.kie.api.runtime.KieSession;
import org.kie.internal.utils.KieHelper;
import org.kie.api.io.ResourceType;
import org.kie.api.builder.Results;
import org.kie.api.builder.Message;
import java.util.List;

public class RuleTemplatesTest {
	
	@Test
	public void testLoginRulesTemplateWithArrays() {
		
		InputStream template = RuleTemplatesTest.class.getResourceAsStream("/sbnz/template/login-rules.drt");
		
		DataProvider dataProvider = new ArrayDataProvider(new String[][] {
			new String[] {"90", "15", "5", "10"},
			new String[] {"120", "10", "10", "5"},
			new String[] {"0", "0", "0", "0"},
		});
		
		DataProviderCompiler converter = new DataProviderCompiler();
		String drl = converter.compile(dataProvider, template);
		
		System.out.println(drl);
		
		KieSession ksession = createKieSessionFromDRL(drl);
		
		doTest(ksession);
		
	}
	
	private KieSession createKieSessionFromDRL(String drl){
        KieHelper kieHelper = new KieHelper();
        kieHelper.addContent(drl, ResourceType.DRL);
        
        Results results = kieHelper.verify();
        
        if (results.hasMessages(Message.Level.WARNING, Message.Level.ERROR)){
            List<Message> messages = results.getMessages(Message.Level.WARNING, Message.Level.ERROR);
            for (Message message : messages) {
                System.out.println("Error: "+message.getText());
            }
            
            throw new IllegalStateException("Compilation errors were found. Check the logs.");
        }
        
        return kieHelper.build().newKieSession();
    }
	
	private void doTest(KieSession ksession){
       
    }

}
