import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { LogViewComponent, TableFilteringExample } from './components/log-view/log-view.component';
import { AlarmViewComponent } from './components/alarm-view/alarm-view.component';
import { GenerateRuleComponent } from './components/generate-rule/generate-rule.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'logView', component: LogViewComponent},
  {path:'regexView',component:TableFilteringExample},
  {path: 'alarmView',component:AlarmViewComponent},
  { path: 'generateRule', component: GenerateRuleComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
