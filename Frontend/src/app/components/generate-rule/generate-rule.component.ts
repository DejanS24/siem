import { Component, OnInit } from '@angular/core';
import { RuleService } from '../../services/rule.service';
import { FormGroup} from '@angular/forms';
import { $ } from 'protractor';

@Component({
  selector: 'app-generate-rule',
  templateUrl: './generate-rule.component.html',
  styleUrls: ['./generate-rule.component.css']
})
export class GenerateRuleComponent implements OnInit {
  public rule : Rule;
  public field : string;
  public fieldVal : string;

  constructor( private ruleService : RuleService) {
    this.ruleService = ruleService;  
  }

  ngOnInit() {
    this.rule = {name:"", conditions:[], occurrences:0,interval:0,unit:"minutes", not:false};
    this.field = "ip";
    this.fieldVal = "";
  }

  andClicked(){
    if (this.addCondition() == false) return;
    this.rule.conditions.push("&&");
    console.log(this.rule);
  }
  
  orClicked(){
    if (this.addCondition() == false) return;
    this.rule.conditions.push("||");
  }
  
  private addCondition(){
    if (this.fieldVal == ""){
      alert("Value can't be empty.")
      return false;
    }
    var valIsString = false;
    if (this.field == "type"){
      if (this.fieldVal == "login"){
        this.fieldVal = "LogType.LOGIN";
      }else if (this.fieldVal == "antivirus"){
        this.fieldVal = "LogType.ANTIVIRUS";
      }else if (this.fieldVal == "payment"){
        this.fieldVal = "LogType.PAYMENT";
      }else {
        alert("Type must be login, antivirus or payment");
        return false;
      }
    }else if (this.field == "status"){
      if (this.fieldVal == "success"){
        this.fieldVal = "Status.SUCCESS";
      }else if (this.fieldVal == "warning"){
        this.fieldVal = "Status.WARNING";
      }else if (this.fieldVal == "error"){
        this.fieldVal = "Status.ERROR";
      }else {
        alert("status must be success, warning or error");
        return false;
      }
    }else if (this.field == "user.username" || this.field == "ip"
            || this.field == "platform"){
      valIsString = true;
    }
    if (valIsString){
      var condition = this.field + ' == "' + this.fieldVal + '"';
    }else{
      var condition = this.field + " == " + this.fieldVal;
    }
    this.rule.conditions.push(condition);
    this.field = "ip", this.fieldVal = "";
  }

  notClicked(){
    console.log("not");
    if (this.rule.not) {
      this.rule.not = false;
    }else{
      this.rule.not = true;
    }
  }

  createRule(){
    console.log(this.rule);
    this.addCondition();
    if (this.rule.conditions.length == 0 || this.rule.name == "" ||
      this.rule.occurrences == 0 || this.rule.interval == 0){
      alert("All fields must be filled correctly."); 
      return;
    }
    
    this.ruleService.create(this.rule).subscribe(success => {console.log(success);},
    error => {console.log(error);});
  }

}

class Rule{
  name: string;
  conditions: string[];
  occurrences: number;
  interval: number;
  unit: string;
  not: boolean;
}