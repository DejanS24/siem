import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateRuleComponent } from './generate-rule.component';

describe('GenerateRuleComponent', () => {
  let component: GenerateRuleComponent;
  let fixture: ComponentFixture<GenerateRuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateRuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
