import { Component, OnInit } from '@angular/core';
import {LogViewService} from 'src/app/services/log-view.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { Observable,throwError } from 'rxjs';
import {forEach} from 'randa';
import {Pipe,PipeTransform} from "@angular/core";
import { MatTableDataSource } from '@angular/material';


@Component({
  selector: 'app-log-view',
  templateUrl: './log-view.component.html',
  styleUrls: ['./log-view.component.css']
})


export class LogViewComponent implements OnInit {
  

  public listOfLogs;
  public searchText;
  public regExp;
  public static copyOfLogs = [];
  
  constructor(private LogViewService: LogViewService) {

      this.listOfLogs = [];
   }

  ngOnInit() {
    
    this.LogViewService.getLogs().subscribe(success => {this.setLogs(success)});
  }

  setLogs(data){
    this.listOfLogs = data;
    
    for(let l of this.listOfLogs){
      console.log(l.type);
    }
  }
  

}


@Component({
  selector: 'log-view.component',
  styleUrls: ['table-filtering-example.css'],
  templateUrl: 'table-filtering-example.html' 
})

export class TableFilteringExample implements OnInit {
  displayedColumns: string[] = ['ip', 'text', 'status', 'type'];
  listOfLogs = [];
  dataSource;
  regExpr:any;
  constructor(private LogViewService: LogViewService) {

    this.listOfLogs = [];

 }

 setLogs(data){
  this.listOfLogs = data;
  this.dataSource = new MatTableDataSource(data);
  this.dataSource.filterPredicate =this.regExprFilter();
  
    for(let l of this.listOfLogs){
      console.log(l.type);
    }
  
  }
  ngOnInit() {
      
    this.LogViewService.getLogs().subscribe(success => {this.setLogs(success)});
    console.log("here");
    
  }

  applyFilter(filterValue: string) {
    console.log(this.listOfLogs);
    
    this.regExpr = new RegExp(filterValue);
    this.dataSource.filter = filterValue;
  }
  regExprFilter()
  { 
    return (data: any, filter: string) => {
        try {

          return this.regExpr.test(data.text)
        } catch (e) {
          return false
        } 
      }
  }

}
