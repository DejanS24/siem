import { Component, OnInit } from '@angular/core';
import {CertificateService} from "../../services/certificate.service";

@Component({
  selector: 'app-generate-certificate',
  templateUrl: './generate-certificate.component.html',
  styleUrls: ['./generate-certificate.component.css']
})
export class GenerateCertificateComponent implements OnInit {
  public IssuerData = {x500Name: "", serialNumber: ""};
  public SubjectData = {name: "", startDate: Date.now(), endDate: Date.now()};
  public CertificateResponse = {issuerData:this.IssuerData, subjectData:this.SubjectData, x500Name:"", type: ""};


  constructor(private certificateService:CertificateService) {
  }

  ngOnInit() {

  }

  public generateCertificate():void{
    console.log(this.CertificateResponse);
    console.log(this.IssuerData);
    console.log(this.SubjectData);
    this.CertificateResponse.issuerData=this.IssuerData;
    this.CertificateResponse.subjectData=this.SubjectData;
    this.certificateService.generateCertificate(this.CertificateResponse);



  }




}

