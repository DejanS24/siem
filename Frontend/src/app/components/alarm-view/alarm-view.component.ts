import { Component, OnInit } from '@angular/core';
import {LogViewService} from 'src/app/services/log-view.service';

@Component({
  selector: 'app-alarm-view',
  templateUrl: './alarm-view.component.html',
  styleUrls: ['./alarm-view.component.css']
})
export class AlarmViewComponent implements OnInit {

  public listOfAlarms;
  constructor(private LogViewService: LogViewService) {

    this.listOfAlarms = [];
 }

  ngOnInit() {
    
    this.LogViewService.getAlarms().subscribe(success => {this.setAlarms(success)});
  }

  setAlarms(data){
    this.listOfAlarms = data;
    
    for(let l of this.listOfAlarms){
      console.log(l.type);
    }
  }
}
