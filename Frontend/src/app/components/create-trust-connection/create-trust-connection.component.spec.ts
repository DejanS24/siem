import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTrustConnectionComponent } from './create-trust-connection.component';

describe('CreateTrustConnectionComponent', () => {
  let component: CreateTrustConnectionComponent;
  let fixture: ComponentFixture<CreateTrustConnectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTrustConnectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTrustConnectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
