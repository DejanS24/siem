import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class JwtUtilsService {
  private jwtHelper: JwtHelperService;

  constructor() {
    this.jwtHelper = new JwtHelperService();
  }

  getRoles(token: string) {
    if(token == ""){
      return [];
    }
    let decodedToken = this.decodeToken(token);
    console.log(this.decodeToken(token));
    return decodedToken.roles;
  }

  getUsername(token: string) {
    if(token == ""){
      return [];
    }
    let decodedToken = this.decodeToken(token);
    console.log(this.decodeToken(token));
    return decodedToken.sub;
  }

  decodeToken(token: string): Token {
    return this.jwtHelper.decodeToken(token);
  }
}

class Token {
  sub: string;
  created: Date;
  roles: Array<string>;
  exp: Date;
}