import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {JwtUtilsService} from './security/jwt-utils.service'

import {AuthenticationService} from './security/authentication.service'
@Injectable({
  providedIn: 'root'
})
export class LogViewService {
  private readonly basePath = 'https://localhost:8080/logView';
  private authService;
  private jwt;
  constructor(private http: HttpClient, authService:AuthenticationService, jwt:JwtUtilsService) {
    this.authService = authService;
    this.http = http;
    this.jwt = jwt
  } 

getLogs(){
  var headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization' : this.authService.getToken() });
  return this.http.get(this.basePath + "/getLogs", {headers, responseType: 'json'});
}


getAlarms(){
  var headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization' : this.authService.getToken() });
  return this.http.get(this.basePath + "/getAlarms", {headers, responseType: 'json'});
}

}
