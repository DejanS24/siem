import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaderResponse, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CertificateService {

  constructor(private http:HttpClient) {
    this.http = http;
  }

  generateCertificate(CertificateDTO){
    var path  = 'http://localhost:8084/api/Certificate/generate'
    console.log(CertificateDTO);
    var headers: HttpHeaders = new HttpHeaders({'Content-Type':'application/json'})
    return this.http.post(path,JSON.stringify(CertificateDTO),{headers, responseType: 'text'}).subscribe(success => {console.log(success);},
      error => {console.log(error);});;
  }
}
