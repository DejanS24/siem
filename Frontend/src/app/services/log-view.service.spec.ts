import { TestBed } from '@angular/core/testing';

import { LogViewService } from './log-view.service';

describe('LogViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LogViewService = TestBed.get(LogViewService);
    expect(service).toBeTruthy();
  });
});
