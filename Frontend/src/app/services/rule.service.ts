import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from './security/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class RuleService {

  constructor(private http: HttpClient, private authService: AuthenticationService) {
    this.http = http;
  }

  create(rule){
    var path = 'https://localhost:8080/rules/dynamic_rule';
    console.log(rule);
    var headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json'});

    console.log('alo');
    return this.http.post(path,JSON.stringify(rule), {headers, responseType: 'text'});      
  }
}
